# GEOL_BIM web app UI

Frontend of the GEOL_BIM web app

## Prerequisites to make it run

Install docker engine and docker compose. On Windows install Docker Desktop with WSL 2 Linux kernel MSI.

You need all 3 repositories as described in section Source-Repositories.

In order to profit from a persistent db storage read the according readme of the db repository.

## Docker-Deployment

### Prerequisites

* Docker (For e.g. Debian see: [https://docs.docker.com/engine/install/debian/#install-using-the-repository](https://docs.docker.com/engine/install/debian/#install-using-the-repository))
* docker-compose (see [https://docs.docker.com/compose/install/#install-compose-on-linux-systems](https://docs.docker.com/compose/install/#install-compose-on-linux-systems))

### Source-Repositories

In order to run a local docker-compose or in production all the following repositories are mandatory:

* [geol_bim-server-ui](https://gitlab.com/CHGEOL/geol_bim-server-ui)
* [geol_bim-server-api](https://gitlab.com/CHGEOL/geol_bim-server-api)
* [geol_bim-server-db](https://gitlab.com/CHGEOL/geol_bim-server-db)

These repositories must be in the same directory.

#### UI

Remind: In a Vue.js app the .env file must be named: `.env.testing`. The suffix `testing` must correspond to the Vue-CLI command `vue-cli-service build --mode testing`. This Vue-CLI command is in `package.json` configured as a Script-Alias `build:test`. Firthermore `.env.testing` must contain an entry like `NODE_ENV=testing`. See more in the [Vue.js Docs](https://cli.vuejs.org/guide/mode-and-env.html#modes).

File location: Repository-Folder

Example:

```env
NODE_ENV=testing  # Change here according to the stage: development, testing or production

COMPOSE_PROJECT_NAME=geol_bim-apisrv

VUE_APP_API_VERSION=0.0.1
VUE_APP_FRONTEND_VERSION=0.0.1

VUE_APP_API_BASE_URL=//localhost:5000/api/0.0/  # Change according to server name. E.g. //geolbim.fhnw.ch/api/0.0/
VUE_APP_TRANSFORM_SUFFIX=transform/
VUE_APP_USER_SUFFIX=user/
VUE_APP_AUTH_SUFFIX=auth/
```

#### API

No special notes here.

File Location: Repository-Folder > `instance` > `.env.test` (Change according to the stage: `dev`, `test` or `prod`)

## Make it run in production

Commands must be executed in the `geol_bim-server-ui` directory.

```bash
docker-compose -f docker-compose-test.yml -p geol_bim-apisrv up -d
```
respectively
```bash
docker-compose -f docker-compose-test.yml -p geol_bim-apisrv up -d --build --force-recreate
```
to be sure everything is rebuilt anew (this does not clear the contents of the DB). You also need to prefix `sudo docker-compose ..` to run it fully privileged.

## Make it run for local development

To develop locally within VSCode do `Reopen in Container` (That is what the .devcontainer.json config file is for.).

This will create all containers (db, api and ui) and open ui container for development in the `geol_bim-server-ui` directory. At this point DB and API are already running if configured correctly. The frontend web server is not started.

In order to start the frontend web server you can either do that in the shell within the ui container or use the ready launch configuration within VSCode.

### In the shell

```bash
npm install
npm build:dev
npm run serve
```
Unfortunately due to the slow file system binding between the container and the local npm install often failes with code 254 and the node_modules/ are fetched incompletely. Just repeat npm install until it succeeds.

Once installed and built it is enough to `npm run serve` each time you want the frontend to run.

### Using the VSCode launch configurations

Just use `Launch via NPM`. If you have not yet installed and built use `NPM install` and `NPM build` first. Use `NPM install` repeatingly until it succeeds.

### Using the VSCode Debugger for the backend together with a running frontend

When just starting the container setup from this repo the API container cannot be used for step-by-step debugging.

In order to achieve step-by-step debugging start the UI container by `Reopen in Container` and then `Launch via NPM` for you have the vue frontend running in the browser. Then open the API repo in another VCCode window and do there `Reopen in Container` of just the API. The already running conatiner setup is reused and that environment comes up. But then you cannot start the API server by the lauch configuration of the VSCode since it is already running (port already in use). You have to do a  `Rebuild Container`. This restarts the DB and API containers (UI continues running) and reopens development in the API container. Doing so the backend is not running and you have to start it using the `Python: Flask` launch configuration, which then attaches the remote debugging properly.

If you only need the backend for debugging (Using /docs as UI instead of the vue frontend) do not `Reopen in Container` here but only in the API repo and launch the `Python: Flask` there.
