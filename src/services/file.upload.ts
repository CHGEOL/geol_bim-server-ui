import { AxiosRequestConfig, AxiosResponse } from 'axios';
import { HttpClient } from '@/utils/HttpClient';
import store from '@/store';

export class FileUpload extends HttpClient {
    public constructor() {
        super(process.env.VUE_APP_API_BASE_URL);
    }

    public userImgUpload= (payload : {UserId: string, formData : FormData}) => {
        //const file : string | File | null = payload.formData.get('photos');
        //let usr_image_type : string = '';

        /*if(file != null && typeof file.type == 'string' && file.type != '') {
            usr_image_type = file.type;
        }*/
        //console.log(file);

        /*for (var key : File of payload.formData.entries()) {
            usr_image_type = key[1].type;
        }*/

        /*const formattedPayload : {usr_image_type: string, formData : FormData} = {
            usr_image_type: 'jpeg',
            formData: payload.formData
        }*/
        const suffix = process.env.VUE_APP_USER_SUFFIX;
        return this.instance.patch(`${suffix}users/${payload.UserId}/img`, payload.formData).then(x => x.data);
    }

    public orgImgUpload= (payload : {OrgId: string, formData : FormData}) => {
        const suffix = process.env.VUE_APP_USER_SUFFIX;
        return this.instance.patch(`${suffix}users/organisations/${payload.OrgId}/img`, payload.formData).then(x => x.data);
    }

    public projectConfigUpload= (payload : {ProjectId: string, formData : FormData}) => {
        const suffix = process.env.VUE_APP_TRANSFORM_SUFFIX;
        return this.instance.patch(`${suffix}projects/${payload.ProjectId}/cfg`, payload.formData).then(x => x.data);
    }

    public projectInputUpload= (payload : {ProjectId: string, formData : FormData}) => {
        const suffix = process.env.VUE_APP_TRANSFORM_SUFFIX;
        return this.instance.patch(`${suffix}projects/${payload.ProjectId}/ipt`, payload.formData).then(x => x.data);
    }

    public blockUpload= (payload : {BlockId: string, formData : FormData}) => {
        const suffix = process.env.VUE_APP_TRANSFORM_SUFFIX;
        return this.instance.patch(`${suffix}blocks/${payload.BlockId}/dta`, payload.formData).then(x => x.data);
    }

}