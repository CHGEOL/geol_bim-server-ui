import { HttpClient } from '@/utils/HttpClient';

export class AuthApi extends HttpClient {
    public constructor() {
        super(process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_AUTH_SUFFIX);
    }

    public login = (payload : {email: string, password: string}) => {
        return this.instance.post('login', payload)
        .then(data => data);
    }

    public logout = () => this.instance.get('logout')
        .then(data => data);

    //public register = (body : any) => this.instance.post('register', { user : body } )
    public register = (body : any) => this.instance.post('register', body )
        .then(data => data);

    public passwordResetRequest = (body : any) => this.instance.post('password/reset', body )
        .then(data => data);

    public resetPassword = (token : string, password : string) => this.instance.patch('password/reset', {token: token, password: password})
        .then(data => data);

    public activateUser = (token : string) => this.instance.patch('register', {token: token})
        .then(data => data);

}