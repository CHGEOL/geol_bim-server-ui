import { HttpClient } from '@/utils/HttpClient';
import { User } from '@/types/User';
import { UserOrganisation } from '@/types/UserOrganisation';
import { UserProject } from '@/types/UserProject';

export class PermissionsApi extends HttpClient {
    public constructor() {
        super(process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_USER_SUFFIX);
    }

    public fetchAllUsersApplication = () => this.instance
    .get<User[]>(`user_applications`).then(data => data);



    public fetchAllUsersOrganisation = (orgId: number) => this.instance
    .get<UserOrganisation[]>(`user_organisations?uog_org_id=${orgId}`).then(data => data);

    public fetchUserAllOrganisations = (usrId: number) => this.instance
    .get<UserOrganisation[]>(`user_organisations?uog_usr_id=${usrId}`).then(data => data);

    public addUserOrganisation = (usrId: number, orgId: number, role: string) => this.instance
    .post<UserOrganisation[]>(`user_organisations`, {"uog_org_id": orgId,"uog_role_code": role,"uog_usr_id": usrId})
    .then(data => data);

    public deleteUserOrganisation = (usrId: number, orgId: number, role: string) => this.instance
    .delete<UserOrganisation[]>(`user_organisations/${usrId}/${orgId}/${role}`).then(data => data);



    public fetchAllUsersProject = (proId: number) => this.instance
    .get<UserProject[]>(`user_projects?upr_pro_id=${proId}`).then(data => data);

    public fetchUserAllProjects = (usrId: number) => this.instance
    .get<UserProject[]>(`user_projects?upr_usr_id=${usrId}`).then(data => data);

    public addUserProject = (usrId: number, proId: number, role: string) => this.instance
    .post<UserProject[]>(`user_projects`, {"upr_pro_id": proId,"upr_role_code": role,"upr_usr_id": usrId})
    .then(data => data);

    public deleteUserProject = (usrId: number, ProId: number, role: string) => this.instance
    .delete<UserProject[]>(`user_projects/${usrId}/${ProId}/${role}`).then(data => data);
}