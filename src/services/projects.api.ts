import { HttpClient } from '@/utils/HttpClient';
import { Project } from '@/types/Project';
import { ProjectRole } from '@/types/ProjectRole';
import { RoleCode } from '@/types/RoleCode';
import Vue from 'vue';

export class ProjectsApi extends HttpClient {
    public constructor() {
        super(process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_TRANSFORM_SUFFIX);
    }

    public fetchProjects = () => this.instance
    .get<Project[]>('projects')
    .then(data => data);

    public fetchProjectsPlus = () => this.instance
    .get<Project[]>('projects?statistics=true')
    .then((res) => res)

    public fetchTemplates = () => this.instance
    .get<Project[]>('projects')
    .then((res) => res.data.filter(function(project : Project) {
            return project.pro_pro_id !== undefined;
        })
    );

    public fetchProject = (projectId: string) => this.instance
    .get<Project>(`projects/${projectId}` )
    .then((data) => {
        return data;
    });

    public fetchProjectFull = (projectId: string) => this.instance
    .get<Project>(`projects/${projectId}?statistics=true&nested=blocks`)
    .then((res) => {
        //console.log("fetchProjectWithProcesses", res);
        return res;
    });

    public createProject = (body: Project) => {
        const { pro_create_date, pro_change_date, pro_create_user, pro_change_user, pro_id, pro_blk_nested, ...created } = body;
        return this.instance.post(`projects`, created)
        .then(data => {
            Vue.$toast.success('Job erfolgreich erstellt.');
            return data;
        });
    }

    public copyProject = (body: Project, projectId : number) => {
        //const { pro_org_name, ...toBeCreated } = body;

        const toBeCreated = { 
            pro_name: body.pro_name, 
            pro_desc: body.pro_desc, 
            pro_org_id: body.pro_org_id,
            //pro_pro_id: body.pro_pro_id, 
            //pro_id: body.pro_pro_id, 
            pro_type_code: body.pro_type_code
        }

        //return this.instance.post('projects/copy', toBeCreated)
        return this.instance.post(`projects/${projectId}/copy`, toBeCreated)
        .then( ( response : any ) => {
            Vue.$toast.success('Job '+toBeCreated.pro_name+' erfolgreich erstellt.');
            return response;
        });
    }

    public editProject = (body: Project) => {
        //console.log("Api project edit", body);
        //spread the body to another variable without pro_id and
        //invalid fields (ES9 Object Rest Operator)
        const { pro_id, pro_version, pro_change_date, pro_change_user, pro_create_date, pro_create_user, pro_blk_nested, ...edited } = body;
        //const edited = {"pro_name": body.pro_name, "pro_org_id": body.pro_org_id};
        //console.log("edited", edited);
        return this.instance.patch(`projects/${body.pro_id}`, edited)
        .then(data => {
            Vue.$toast.success('Job erfolgreich bearbeitet.');
            return data;
        });
    }

    public deleteProject = (projectId: number) => this.instance.delete(`projects/${projectId.toString()}` )
    .then((data) => {
        Vue.$toast.success('Job erfolgreich gelöscht.');
        return data;
    });


    public fetchRoles = (projectId: string) => this.instance.get<ProjectRole[]>(`roles_project?rop_pro_id=${projectId}`)
    .then(data => data);

    public fetchAllProjectRoles = () => this.instance.get<ProjectRole[]>(`roles_project`)
    .then(data => data);

    public fetchAllRoleCodes = () => this.instance.get<RoleCode[]>('codes?cod_col_name=role_project')
    .then(data => data);

    public createProjectRole = (body: Object) => this.instance.post('roles_project', body)
    .then((data) => {
        Vue.$toast.success('Rolle erfolgreich erstellt.');
        return data;
    });

    public editProjectRole = (body: ProjectRole) => {
        //console.log("Api project edit", body);
        //spread the body to another variable without pro_id and
        //invalid fields (ES9 Object Rest Operator)
        const {rop_role_code_code, rop_role_code_name, rop_create_date, rop_pro_name, rop_create_user, rop_change_user, rop_change_date, ...edited } = body;
        //const edited = {"pro_name": body.pro_name, "pro_org_id": body.pro_org_id};
        //console.log("edited", edited);
        return this.instance.patch(`roles_project/${body.rop_id}`, edited)
        .then((data) => {
            Vue.$toast.success('Rolle erfolgreich bearbeitet.');
            return data;
        });
    }

    public deleteProjectRole = (projectRoleId: string) => this.instance.delete(`roles_project/${projectRoleId}` )
    .then((data) => {
        Vue.$toast.success('Rolle erfolgreich gelöscht.');
        return data;
    });
}

