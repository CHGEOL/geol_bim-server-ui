import { HttpClient } from '@/utils/HttpClient';
import { Block } from '@/types/Block';
import Vue from 'vue';

export class BlockApi extends HttpClient {
    public constructor() {
        super(process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_TRANSFORM_SUFFIX);
    }

    public fetchBlocks = () => this.instance
    .get<Block[]>(`blocks` )
    .then(data => data);

    public fetchBlock = (OrgId: string) => this.instance
    .get<Block>(`blocks/${OrgId}` )
    .then(data => data);

    public fetchMultipleBlock = (blkIds : number[]) => {
        let requests = new Array();
        blkIds.forEach( (id:number) => {
            requests.push( this.instance.get(`blocks/`+id.toString()) );
        })
        return Promise.all(requests);
    }

    public fetchBlocksByProject = (projectId: number) => this.instance
    .get<Block[]>(`blocks?blk_pro_id=${projectId}`)
    .then(data => data);

    public editBlock = (body: Block) => {
        //console.log("Api user edit", body);
        //spread the body to another variable without pro_id and
        //other invalid fields (ES9 Object Rest Operator)
        const { blk_create_date, blk_change_date, blk_create_user, blk_change_user, ...edited } = body;
        // We must patch blk_data only by an appropriate upload never by the normal patch. But we allow to clear it.
        if (edited.blk_data && edited.blk_data != '') {
            edited.blk_data = undefined;
        }
        //console.log("edited", edited);
        // TODO why does it make sense to return that Promise - anyway the caller cannot get the edited data from the server - ?
        return this.instance.patch(`blocks/${body.blk_id}`, edited)
        .then(data => {
        //Vue.$toast.success('Block erfolgreich bearbeitet.');
        return data;
        });
    }

    public deleteBlock = (OrgId: number) => this.instance
    .delete(`blocks/${OrgId}` )
    .then(data => {
        Vue.$toast.success('Block erfolgreich gelöscht.');
        return data;
    });

    public createBlock = (body: Block) => {
        //spread the body to another variable without usr_id and
        //other invalid fields (ES9 Object Rest Operator)
        const { blk_create_date, blk_change_date, blk_create_user, blk_change_user, blk_id, ...created } = body;
        return this.instance.post(`blocks`, created)
        .then(data => {
            Vue.$toast.success('Block erfolgreich erstellt.');
            return data;
        });
    }
}