import { HttpClient } from '@/utils/HttpClient';
import { Organisation } from '@/types/Organisation';
import Vue from 'vue';

export class OrganisationApi extends HttpClient {
    public constructor() {
        super(process.env.VUE_APP_API_BASE_URL + process.env.VUE_APP_USER_SUFFIX);
    }

    public fetchOrganisations = () => this.instance
    .get<Organisation[]>(`organisations` )
    .then(data => data);

    public fetchOrganisation = (OrgId: string) => this.instance
    .get<Organisation>(`organisations/${OrgId}` )
    .then(data => data);

    public fetchMultipleOrganisation = (orgIds : number[]) => {
        let requests = new Array();
        orgIds.forEach( (id:number) => {
            requests.push( this.instance.get(`organisations/`+id.toString()) );
        })
        return Promise.all(requests);
    }

    public editOrganisation = (body: Organisation) => {
        //console.log("Api user edit", body);
        //spread the body to another variable without pro_id and
        //other invalid fields (ES9 Object Rest Operator)
        const { org_create_date, org_change_date, org_create_user, org_change_user, org_image, ...edited } = body;
        //console.log("edited", edited);
        return this.instance.patch(`organisations/${body.org_id}`, edited)
        .then(data => {
        Vue.$toast.success('Organisation erfolgreich bearbeitet.');
        return data;
        });
    }

    public deleteOrganisation = (OrgId: number) => this.instance
    .delete(`organisations/${OrgId}` )
    .then(data => {
        Vue.$toast.success('Organisation erfolgreich gelöscht.');
        return data;
    });

    public createOrganisation = (body: Organisation) => {
        //spread the body to another variable without usr_id and
        //other invalid fields (ES9 Object Rest Operator)
        const { org_create_date, org_change_date, org_create_user, org_change_user, org_image, org_id, ...created } = body;
        return this.instance.post(`organisations`, created)
        .then(data => {
            Vue.$toast.success('Organisation erfolgreich erstellt.');
            return data;
        });
    }
}