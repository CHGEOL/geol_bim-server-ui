import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'
import { Project } from '@/types/Project';

@Module//({ namespaced: true, name: 'new.project' })
export default class CanvasModule extends VuexModule {
    _canvasProject : Project = new Project();

    get canvasProjectName() {
        return this.canvasProject.pro_name;
    }

    get canvasProjectDescription() {
        return this.canvasProject.pro_desc;
    }

    get canvasProjectOrganisationId() {
        return this.canvasProject.pro_org_id;
    }

    get canvasProject() {
        return this._canvasProject;
    }


    @Action
    resetCanvasProject() {
        this.context.commit('RESET_CANVAS_PROJECT');
    }


    @Action
    setupCanvasProject(refProjectId : number) {
        if(refProjectId != undefined) {
            let canvasProject = new Project();
            // @ts-ignore
            canvasProject.pro_pro_id = refProjectId;
            this.context.commit('SET_CANVAS_PROJECT', canvasProject);
        }
        else {
            // TODO use reference project 
        }
    }

    @Action
    updateCanvasProject(project : Project) {
        this.context.commit("SET_CANVAS_PROJECT", project);
    }

    @Action
    setCanvasProjectName(name : string) {
        this.context.commit("SET_CANVAS_PROJECT_NAME", name);
    }

    @Action
    setCanvasProjectDescription(desc : string) {
        this.context.commit("SET_CANVAS_PROJECT_DESCRIPTION", desc);
    }

    @Action
    setCanvasProjectOrganisationId(organisationId : number) {
        this.context.commit("SET_CANVAS_PROJECT_ORGANISATION_ID", organisationId);
    }

    /**
     * 
     * MUTATIONS
     * 
     */

    @Mutation
    SET_CANVAS_PROJECT(project : Project) {
        this._canvasProject = project;
    }

    @Mutation
    SET_CANVAS_PROJECT_NAME(name : string) {
        this._canvasProject.pro_name = name;
    }

    @Mutation
    SET_CANVAS_PROJECT_DESCRIPTION(desc : string) {
        this._canvasProject.pro_desc = desc;
    }

    @Mutation
    SET_CANVAS_PROJECT_ORGANISATION_ID(orgId : number) {
        this._canvasProject.pro_org_id = orgId;
    }

    @Mutation
    RESET_CANVAS_PROJECT() {
        // nothing special to do
    }
}
