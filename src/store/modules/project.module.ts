import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'
import { Project } from '@/types/Project';
import { ProjectsApi } from '@/services/projects.api';

@Module//({ namespaced: true, name: 'auth' })
export default class ProjectModule extends VuexModule {
    workingProject : Project = new Project(); //Project currently being created
    selectedProject : Project = new Project(); //Project selected for browsing its details

    get currentProject() : Project {
        return this.selectedProject;
    }

    get projectIdDefined() : boolean {
        return this.selectedProject.pro_id != undefined;
    }

    get newProject() : Project {
        return this.workingProject;
    }

    /*--------MUTATIONS----------*/

    @Mutation
    SET_PROJECT_NAME(pro_name : string){
        this.workingProject.pro_name = pro_name;
    }

    @Mutation
    SET_CURRENT_PROJECT(project : Project){
        this.selectedProject = project;
    }

    @Mutation
    SET_CURRENT_PROJECT_NAME(pro_name : string){
        this.selectedProject.pro_name = pro_name;
    }

    @Mutation
    SET_CURRENT_PROJECT_DESCRIPTION(pro_desc : string){
        this.selectedProject.pro_desc = pro_desc;
    }

    @Mutation
    UNSET_CURRENT_PROJECT() {
        this.selectedProject = new Project();
    }

    @Mutation
    RESET_WORKING_PROJECT() {
        this.workingProject = new Project();
    }


    @Mutation
    SET_NEW_PROJECT_ORGANISATION(org : { pro_org_id: number, pro_org_name: string }){
        this.workingProject.pro_org_id = org.pro_org_id;
        //this.workingProject.pro_org_name = org.pro_org_name;
    }

    @Mutation
    SETUP_NEW_PROJECT_REFERENCE(projectId: number) {
        // @ts-ignore
        this.workingProject.pro_pro_id = projectId;
    }


    /**
     * --------ACTIONS--------
     * */

    @Action
    getProject(projectId : number) {
        const api = new ProjectsApi();
        api.fetchProject(projectId.toString())
        .then( res => {
            this.context.commit('SET_CURRENT_PROJECT', res);
        })
    }

    @Action
    getProjectFull(projectId : number) {
        const api = new ProjectsApi();
        api.fetchProjectFull(projectId.toString())
        .then( res => {
            const project = res;
            const payload = {project: project};
            this.context.dispatch('setCurrentProjectFull', payload);
        })
    }

    @Action
    setProjectName(pro_name : string) {
        this.context.commit('SET_PROJECT_NAME', pro_name);
    }

    @Action
    setCurrentProject(project : Project) {
        this.context.commit('SET_CURRENT_PROJECT', project);
    }

    @Action
    setCurrentProjectFull(payload: {project : Project}) {
        this.context.commit('SET_CURRENT_PROJECT', payload.project);
    }

    @Action
    unsetCurrentProject() {
        this.context.commit('UNSET_CURRENT_PROJECT');
    }

    @Action
    resetWorkingProject() {
        this.context.commit('RESET_WORKING_PROJECT');
    }

    @Action
    setNewProjectOrganisation(org : { pro_org_id: number, pro_org_name: string }) {
        this.context.commit('SET_NEW_PROJECT_ORGANISATION', org);
    }

    @Action
    setupNewProjectReference(projectId: number) {
        this.context.commit('SETUP_NEW_PROJECT_REFERENCE', projectId);
    }

}