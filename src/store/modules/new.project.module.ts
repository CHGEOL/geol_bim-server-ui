import { VuexModule, Module, Mutation, Action } from 'vuex-module-decorators'

@Module//({ namespaced: true, name: 'new.project' })
export default class NewProjectModule extends VuexModule {
    // nothing special remained in here
}
