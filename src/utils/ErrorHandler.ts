import { APIError } from '@/types/APIError';

export class ErrorHandler {
    field_dict_de : { [key: string]: string } = {
        'org_name':'Organisation Name',
        'org_desc':'Beschreibung Organisation',
        'pro_name':'Projektname',
        'pro_desc':'Projektbeschreibung',
        'rop_name':'Projektrolle Name',
        'rop_desc':'Projektrolle Beschreibung',
        'usr_name':'Anmeldename',
        'usr_name_first':'Benutzer Vorname',
        'usr_desc':'Benutzer Beschreibung',
        'usr_email':'Benutzer E-Mail',
        'pro_org_id':'Projekt Organisation-ID',
        'pro_org_name': 'Projekt Organisation Name',
        'pro_pers' : 'Projekt Personal flag',
        'usr_login_date' : "Benutzer Login-Datum",
        'usr_login_ip' : 'Benutzer IP-address',
        'usr_org_pers' : 'Benutzer Organisation',
        'usr_org_pers_id' : 'Benutzer Organisation-ID',
        'usr_org_pers_name' : 'Benutzer Organisation Name',
        'usr_pw' : 'Benutzer Kennwort',
    };

    field_dict : { [key: string]: string } = {
        'org_name':'Organisation name',
        'org_desc':'Organisation description',
        'pro_name':'Project name',
        'pro_desc':'Project description',
        'rop_name':'Project Role name',
        'rop_desc':'Project Role description',
        'usr_name':'User last name',
        'usr_name_first':'User first name',
        'usr_desc':'User description',
        'usr_email':'User e-mail',
        'pro_org_id':'Project Organisation Identifier',
        'pro_org_name': 'Project Organisation name'
    };
      

    process(error : APIError) : string {
        //console.log("Errorhandler process", error);
        let message = "";

        if(error.message != null) {
            message = error.message;
            if(error.internal_code != '') {
                message += " (internal code: "+ error.internal_code+")";
            }
            //message += "<br />";
        }

        if(error.details != null && Object.keys(error.details).length > 0) {
            const keys = Object.keys(error.details);
            for(let i=0; i<keys.length; i++) {
                let key_name = keys[i];
                if(error.details[key_name] != null) {
                    //let msg = "<strong>" +this.field_dict_de[key_name] + "</strong> is " + error.details[key_name].toLowerCase() + "<br />";
                    let msg = this.field_dict_de[key_name] + " is " + error.details[key_name].toLowerCase();
                    message += msg;
                }
            }
        }

        return message;
    }
}
