export enum AppStates {
    DEFAULT,
    LOGIN,
    PROJECT_DASHBOARD,
    ROLES_PAGE,
    USER_PROFILE,
    PROJECTS_PAGE
}