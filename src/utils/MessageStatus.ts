export enum MessageStatus {
    INFO,
    LOG,
    DEBUG,
    WARNING,
    ERROR,
    SUCCESS
}