import axios, { AxiosInstance, AxiosResponse, AxiosRequestConfig } from 'axios';
//import store from '@/store';
import router from '@/router';
import log from '@/utils/logger';
import { AppStates } from '@/utils/AppStates';
import { ErrorHandler } from '@/utils/ErrorHandler';
import Vue from 'vue';

declare module 'axios' {
  interface AxiosResponse<T = any> extends Promise<T> {}
}

export abstract class HttpClient {
  protected readonly instance: AxiosInstance;

  public constructor(baseURL: string) {
    this.instance = axios.create({
      baseURL,
    });
    //console.log('HttpClient', baseURL);
    this._initializeRequestInterceptor();
    this._initializeResponseInterceptor();
  }

  private _initializeRequestInterceptor = () => {
    this.instance.interceptors.request.use(
      this._handleRequest,
      this._handleError,
    );
  };

  private _handleRequest = (config: AxiosRequestConfig) => {
    
    //const token = store.getters.sessionToken;
    const token = localStorage.getItem('token');
    if(token != '' && token != undefined) {
      config.headers['Authorization'] = `Bearer ${token}`;
    }

    //const projectId = router.currentRoute.params.projectId || store.getters.currentProject.pro_id;
    const projectId = router.currentRoute.params.projectId;
    //const projectId = store.getters.currentProject.pro_id;
    //console.log("HTTP Client Handle Request", projectId);
    if(projectId != '' && projectId != undefined) {
      config.headers['X-ProjectId'] = projectId;
    }
    return config;
  };

  private _initializeResponseInterceptor = () => {
    this.instance.interceptors.response.use(
      this._handleResponse,
      this._handleError,
      //() => { console.log("Response error handler in interceptor  ")}
    );
  };

  private _handleResponse = ({ data }: AxiosResponse) => {
    // Any status code that lie within the range of 2xx cause this function to trigger
    // Do something with response data
    // console.log("HttpClient Response Interceptor", data);
    return data;
  };

  protected _handleError = (error: any) => {
    if (401 === error.response.status) {
        /*if(error.response.data.internal_code == 'AUTH') {
            Vue.$toast.error(error.response.data.message);
            return Promise.reject(error.response.data.message);
        }
        else */if( (error.response.data.internal_code == 'AUTH' 
        || error.response.data.internal_code == 'AUTH_EXP_TOK' 
        || error.response.data.internal_code == 'AUTH_BE_TOK' 
        || error.response.data.internal_code == 'AUTH_INV_TOK'
        || error.response.data.internal_code == 'AUTH_NO_TOK')
        ) {
            //&& store.getters.currentAppState != AppStates.LOGIN) {
            //console.log('_handleError', error);
            const redirectPath = router.currentRoute.fullPath;
            //store.dispatch('logoutWithError', redirectPath);
            router.push({
                name: 'login',
                query: { redirect: redirectPath } 
            })
            .catch(err => {
                log.error('[Navigation Error] :' + err);
            });
        }
        else if(error.response.data.internal_code == 'AUTH_PRO') {
            Vue.$toast.error(error.response.data.message);
            history.back();
            //TO DO: delay the error message to be displayed after the history.back() command
            //TO DO BETTER: implement a Router Guard to even prevent such situation in the first place
        }
        else {
            Vue.$toast.error(error.response.data.message);
            return Promise.reject(error.response.data.message);
        }
    }
    else if(503 === error.response.status) {
      router.push({
        name: 'maintenance'
      })
    }
    else {
        const errorHandler : ErrorHandler= new ErrorHandler();
        error.response.data.message = errorHandler.process(error.response.data);
        Vue.$toast.error(error.response.data.message);
        return Promise.reject(error.response.data);
    }
  };
}