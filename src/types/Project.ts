import { Block } from '@/types/Block';

interface ProjectInterface {
    pro_id?: string,
    pro_pro_id?: number | undefined, //reference Project
    pro_org_id?: number,
    pro_name: string,
    pro_desc?: string,
    pro_output?: string | undefined,
    pro_type_code?: string,
    pro_version?: string,
    pro_change_date?: string, 
    pro_change_user?: string, 
    pro_create_date?: string, 
    pro_create_user?: string,
    pro_blk_nested: Block[]
}

export class Project implements ProjectInterface {
    pro_name = '';

    pro_id = '-1';
    pro_pro_id = undefined; // Using the default Reference Project ID
    pro_org_id = -1;
    pro_desc = '';
    pro_output? = undefined;
    pro_type_code = 'pro-usr';
    pro_version = '';
    pro_change_date = ''; 
    pro_change_user = ''; 
    pro_create_date = ''; 
    pro_create_user = '';
    pro_blk_nested = new Array<Block>();

    constructor() {
        return this;
    }
}