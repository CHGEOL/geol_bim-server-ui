export interface RoleCodeInterface {
    cod_code: string,
    cod_col_id: number,
    cod_col_name: string,
    cod_id: string,
    cod_name: string,
    cod_seq: number
}

export class RoleCode implements RoleCodeInterface {
    cod_code = '';
    cod_col_id = -1;
    cod_col_name = '';
    cod_id = '';
    cod_name = '';
    cod_seq = -1;

    constructor() {
        return this;
    }
}