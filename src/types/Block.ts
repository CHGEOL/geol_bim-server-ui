
export interface BlockInterface {
    blk_id: number,
    blk_pro_id: number,
    blk_type_code: string,
    blk_data?: string,
    blk_mime_type?: string,
    blk_heading?: string,
    blk_section?: string,
    blk_state_code: string,
    blk_state_percent?: number,
    blk_state_message?: string,
    blk_seq: number,
    blk_create_date?: string, 
    blk_create_user?: string,  
    blk_change_user?: string,  
    blk_change_date?: string
}

export class Block implements BlockInterface {
    blk_id = -1;
    blk_pro_id = -1;
    blk_type_code = '';
    blk_data? = undefined; // With '' the jsonify on the server will cause troubles
    blk_mime_type? = '';
    blk_heading? = '';
    blk_section? = '';
    blk_state_code = '';
    blk_state_percent? = -1;
    blk_state_message? = '';
    blk_seq = -1;
    blk_create_date = '';
    blk_create_user = '';
    blk_change_user = '';
    blk_change_date = ''

    constructor(type_code? : string, state_code? : string) {
        if(type_code) this.blk_type_code = type_code;
        if(state_code) this.blk_state_code = state_code;
        return this;
    }
}