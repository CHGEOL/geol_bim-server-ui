export interface UserProjectInterface {
    upr_change_date?: string,
    upr_change_user?: string,
    upr_create_date?: string,
    upr_create_user?: string,
    upr_pro_id: number,
    upr_pro_name?: string,
    upr_role_code: string,
    upr_roles_codes?: string[],
    upr_usr_id: number,
    upr_usr_name?: string,
    upr_usr_name_first?: string
}

export class UserProject implements UserProjectInterface {
    upr_change_date? = '';
    upr_change_user? = '';
    upr_create_date? = '';
    upr_create_user? = '';
    upr_pro_id = -1;
    upr_pro_name? = '';
    upr_role_code = '';
    upr_roles_codes? = new Array<string>();
    upr_usr_id = -1;
    upr_usr_name? = '';
    upr_usr_name_first? = '';

    constructor() {
        return this;
    }
}