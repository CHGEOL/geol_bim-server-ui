export interface ProjectRoleInterface {
    rop_change_date? : string,
    rop_change_user? : string,
    rop_create_date? : string,
    rop_create_user? : string,
    rop_desc? : string,
    rop_id : number,
    rop_name : string,
    rop_pro_id : number,
    rop_pro_name? : string,
    rop_role_code? : string,
    rop_role_code_code? : string,
    rop_role_code_name? : string
}

export class ProjectRole implements ProjectRoleInterface {
    rop_change_date? = '';
    rop_change_user? = '';
    rop_create_date? = '';
    rop_create_user? = '';
    rop_desc? = '';
    rop_id = -1;
    rop_name = '';
    rop_pro_id = -1;
    rop_pro_name? = '';
    rop_role_code? = '';
    rop_role_code_code? = '';
    rop_role_code_name? = '';

    constructor() {
        return this;
    }
}