export interface OrganisationInterface {
    org_id: number,
    org_name: string,
    org_desc?: string,
    org_create_date?: string, 
    org_change_date?: string, 
    org_create_user?: string, 
    org_change_user?: string, 
    org_image?: string
}

export class Organisation implements OrganisationInterface {
    org_id = -1;
    org_name = '';
    org_desc? = '';
    org_create_date? = ''; 
    org_change_date? = ''; 
    org_create_user? = ''; 
    org_change_user? = ''; 
    org_image? = '';

    constructor() {
        return this;
    }
}