export interface APIError {
    internal_code: string,
    message: string,
    code: number,
    details: { [key: string]: string }
}