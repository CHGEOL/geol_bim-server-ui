export interface UserOrganisationInterface {
    uog_change_date?: string,
    uog_change_user?: string,
    uog_create_date?: string,
    uog_create_user?: string,
    uog_org_id:	number,
    uog_org_name?: string,
    uog_role_code: string,
    uog_roles_codes?: string[],
    uog_usr_id:	number,
    uog_usr_name?: string,
    uog_usr_name_first?: string
}

export class UserOrganisation implements UserOrganisationInterface {
    uog_change_date? = '';
    uog_change_user? = '';
    uog_create_date? = '';
    uog_create_user? = '';
    uog_org_id = -1;
    uog_org_name? = '';
    uog_role_code = '';
    uog_roles_codes? = new Array<string>();
    uog_usr_id = -1;
    uog_usr_name? = '';
    uog_usr_name_first? = '';

    constructor() {
        return this;
    }
}