import Vue from 'vue';
import router from './router';
import store from './store';
import './filters/TextFilters.ts';
import App from './App.vue';

import logger from './utils/logger';
Vue.use(logger);

import Toast, { POSITION, TYPE } from "vue-toastification";
// Import the CSS or use your own!
import "vue-toastification/dist/index.css";

const options = {
    position: POSITION.BOTTOM_RIGHT,
    transition: "Vue-Toastification__fade",
    maxToasts: 20,
    newestOnTop: true,
    toastDefaults: {
        // ToastOptions object for each type of toast
        [TYPE.ERROR]: {
            timeout: 6000,
            //showCloseButtonOnHover: true
        },
        [TYPE.SUCCESS]: {
            timeout: 3000,
            closeButton: false,
        }    
    }
};

Vue.use(Toast, options);

//import customLogger from './utils/custom-logger';
//Vue.use(customLogger);

//Vue.config.productionTip = true;
//Vue.use(require('vue-moment'));

const currentLog = logger;

Vue.config.errorHandler = function (err, vm, info) {
    // handle error
    // `info` is a Vue-specific error info, e.g. which lifecycle hook
    // the error was found in. Only available in 2.2.0+
    logger.error('[Global Error Handler]: Error in ' + info + ': ' + err);
}

/**
 * Prevent logging already processed unhandled rejection in console
 */
window.addEventListener('unhandledrejection', event => {
    if (event.reason && event.reason.ignoreUnhandledRejection) {
        event.preventDefault();
    }
    /*else {
        logger.error('['+ event.type +']: ' + event.reason);
        event.preventDefault();
        event.stopPropagation();
    }*/
});

new Vue({
    router,
    store,
    render: (h) => h(App),
}).$mount('#app');

import { StatusRetriever } from '@/utils/StatusRetriever';
import { PermissionsRefresher } from '@/utils/PermissionsRefresher';
//import log from '@/utils/logger';

router.beforeEach((to, from, next) => {
    //console.log('beforeEachMaintenance', to.path, store.getters.maintenanceModeActive);
    //console.log(`Env is ${process.env.NODE_ENV}`);
    if(to.name != 'maintenance') {
        const api = new StatusRetriever();
        api.fetchStatus()
        .then(res => {
            if(res.api_version != process.env.VUE_APP_API_VERSION  && to.path !== '/maintenance') {
                store.dispatch('setMaintenanceMode', true);
                /* next({
                    name: 'maintenance'
                }); */
                router.push({
                    name: 'maintenance'
                }).catch(() => {});
            }
            else {
                next();
            }
        })
        .catch(error => {
            if (503 === error.response.status || 404 === error.response.status) {
                store.dispatch('setMaintenanceMode', true);
                /* next({
                    name: 'maintenance'
                }); */
                router.push({
                    name: 'maintenance'
                }).catch(() => {});
            }
        });
    }
    else {
        next();
    }
});

router.beforeEach((to, from, next) => {
    //console.log("checkAuth");
    //console.log('beforeEachLogin', to.path, store.getters.maintenanceModeActive);
    if (to.matched.some(record => record.meta.notAuthRequired)) {
        if(store.getters.isLoggedIn && to.name != 'maintenance') {
            //console.log("router beforeEach - not logged in");
            next({
                name: 'project-list'
            })
        } 
        else {
            // this route doesn't requires auth, so we can proceed
            next(); // make sure to always call next()!
        }
    } else {
        // this route requires auth, check if logged in
        // if not, redirect to login page.
        // we check directly in localStorage to avoid dependency on the Vuex store
        //if (!store.getters.isLoggedIn) {
        const localUser = localStorage.getItem('user');
        //console.log("router.beforeEach not logged in", to.matched)
        if (localUser == null) {
            console.log("router beforeEach - not logged in");
            /* next({
                name: 'login',
                query: { redirect: to.fullPath }
            }) */
            if(to.matched.some(record => record.meta.notAuthRequired)) {
                next();
            }
            else {
                next({
                    name: 'login',
                    query: { redirect: to.fullPath }
                })
            }
            //})
        } else {
            //if the user is logged in, we call a special util class PermissionsRefresher that is independent 
            //of HttpClient (hence independent from the router itself to avoid circular reference)
            //to get the latest permissions and update the Vuex store with them
            const user = JSON.parse(localUser);
            const api = new PermissionsRefresher();
            api.fetchUserPermissions(user.usr_id)
            .then(res => {
                //console.log("permissions refresher response", res);
                store.dispatch('setUserPermissions', res);
                next();
            })
            .catch(error => {
                if (401 === error.response.status) {
                    //console.log('catch error', error.response.data.internal_code);
                    if( (error.response.data.internal_code == 'AUTH' 
                        || error.response.data.internal_code == 'AUTH_NO_TOK' 
                        || error.response.data.internal_code == 'AUTH_EXP_TOK' 
                        || error.response.data.internal_code == 'AUTH_BE_TOK' 
                        || error.response.data.internal_code == 'AUTH_INV_TOK')
                        //&& store.getters.currentAppState != AppStates.LOGIN
                        ) {
                            //console.log('_handleError', error.toJSON());
                            Vue.$toast.error(error.response.data.message);
                            const redirectPath = router.currentRoute.fullPath;
                            store.dispatch('logoutWithError', redirectPath);
                            /* router.push({ */
                            next({
                                name: 'login',
                                query: { redirect: store.getters.loginRedirect } 
                            })
                            /* .catch(err => {
                                log.error('[Navigation Error] :' + err);
                            }); */
                    }
                }
            });
            //next();
        }
    }
});