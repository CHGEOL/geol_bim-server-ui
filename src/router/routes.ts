import { Route } from 'vue-router';
  
const routes = [
    {
        path: '/',
        props: true,
        //TODO: replace with the actual homepage or login form
        //component: () => import(/* webpackChunkName: "projects" */ '@/pages/Projects.vue'),
        redirect: '/projects'
    },
    {
        path: '/projects',
        props: {context: 'project-list'},
        name: 'projects',
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () => import(/* webpackChunkName: "projects" */ '@/pages/PageProjects.vue'),
        redirect: {
            name: 'project-list'
        },
        children: [
            {
                path: 'recent',
                props: {context: 'project-list'},
                name: 'projects-recent',
                component: () => import(/* webpackChunkName: "projectList" */ '@/components/project/UiProjectList.vue'),
            },
            {
                path: 'my-organisation',
                props: {context: 'project-list'},
                name: 'projects-organisation',
                component: () => import(/* webpackChunkName: "projectList" */ '@/components/project/UiProjectList.vue'),
            },
            {
                path: 'public',
                props: {context: 'project-list'},
                name: 'projects-public',
                component: () => import(/* webpackChunkName: "projectList" */ '@/components/project/UiProjectList.vue'),
            },
            {
                path: 'list',
                props: {context: 'project-list'},
                name: 'project-list',
                component: () => import(/* webpackChunkName: "projectList" */ '@/components/project/UiProjectList.vue'),
            }
        ]
    },
    {
        path: '/projects/new',
        props: {context: 'project-list'},
        //props: (route: { params: { projectId: number; }; }) => ({ projectId: Number(route.params.projectId) }),
        name: 'project-new',
        component: () => import(/* webpackChunkName: "projectNew" */ '@/components/project/UiProjectNew.vue'),
    },
    {
        path: '/projects/:projectId/new',
        props: true,
        //props: (route: { params: { projectId: number; }; }) => ({ projectId: Number(route.params.projectId) }),
        name: 'project-new-from-template',
        component: () => import(/* webpackChunkName: "projectNew" */ '@/components/project/UiProjectNew.vue'),
    },
    {
        path: '/projects/:projectId',
        //props: (route: Route) => ({ projectId: Number(route.params.projectId) }),
        props: true,
        component: () => import(/* webpackChunkName: "projectDetails" */ '@/components/project/UiProjectDetails.vue'),
        redirect: 'dashboard',
        children: [
            {
                path: 'dashboard',
                props: true,
                name: 'project-dashboard',
                component: () => import(/* webpackChunkName: "projectDashboard" */ '@/components/project/UiProjectDashboard.vue'),
            },
            {
                path: 'roles',
                name: 'project-roles',
                props: true,
                component: () => import(/* webpackChunkName: "projectRoles" */ '@/components/project/UiProjectRoles.vue'),
                children: [
                    {
                        path: ':roleId',
                        props: (route: Route) => ({ roleId: Number(route.params.roleId) }),
                        name: 'role-details',
                        component: () => import(/* webpackChunkName: "projectRoles" */ '@/components/project/UiProjectRoles.vue'),
                    }
                ],
            },
            {
                path: 'permissions',
                props: true,
                name: 'project-permissions',
                component: () => import(/* webpackChunkName: "projectPermissions" */ '@/components/project/UiProjectUsers.vue'),
            }
        ]
    },
    {
        path: '/templates',
        props: {context: 'template-list'},
        name: 'templates',
        component: () => import(/* webpackChunkName: "templates" */ '@/pages/PageTemplates.vue'),
        redirect: {
            name: 'template-list'
        },
        children: [
            {
                path: 'recent',
                props: {context: 'template-list'},
                name: 'templates-recent',
                component: () => import(/* webpackChunkName: "projectList" */ '@/components/project/UiProjectList.vue'),
            },
            {
                path: 'my-organisation',
                props: {context: 'template-list'},
                name: 'templates-organisation',
                component: () => import(/* webpackChunkName: "projectList" */ '@/components/project/UiProjectList.vue'),
            },
            {
                path: 'public',
                props: {context: 'template-list'},
                name: 'templates-public',
                component: () => import(/* webpackChunkName: "projectList" */ '@/components/project/UiProjectList.vue'),
            },
            {
                path: 'new',
                props: {context: 'template-list'},
                name: 'template-new',
                component: () => import(/* webpackChunkName: "projectNew" */ '@/components/project/UiProjectNew.vue'),
            },
            {
                path: 'list',
                props: {context: 'template-list'},
                name: 'template-list',
                component: () => import(/* webpackChunkName: "projectList" */ '@/components/project/UiProjectList.vue'),
            }
        ]
    },
    {
        path: '/references',
        props: {context: 'reference-list'},
        name: 'references',
        component: () => import(/* webpackChunkName: "references" */ '@/pages/PageReferences.vue'),
        redirect: {
            name: 'reference-list'
        },
        children: [
            {
                path: 'list',
                props: {context: 'reference-list'},
                name: 'reference-list',
                component: () => import(/* webpackChunkName: "referencesList" */ '@/components/project/UiProjectList.vue'),
            }
        ]
    },
    
    {
        path: '/templates/:projectId',
        props: true,
        //props: (route: { params: { projectId: Number; }; }) => ({ projectId: Number(route.params.projectId) }),
        name: 'template-details',
        component: () => import(/* webpackChunkName: "templateDetails" */ '@/components/project/UiTemplateDetails.vue')
    },

    {
        path: '/users',
        props: true,
        name: 'users',
        component: () => import(/* webpackChunkName: "users" */ '@/pages/PageUsers.vue'),
        redirect: {
            name: 'user-profile'
        },
        children: [
            {
                path: 'list',
                props: true,
                name: 'users-list',
                component: () => import(/* webpackChunkName: "usersList" */ '@/components/user/UiUserList.vue'),
            },
            {
                path: 'new',
                props: true,
                name: 'user-creation',
                component: () => import(/* webpackChunkName: "userNew" */ '@/components/user/UiUserNew.vue'),
            },
            {
                path:'organisation/:orgId',
                props: true,
                name: 'users-org-perm',
                component: () => import(/* webpackChunkName: "userOrganisationPermissions" */ '@/components/organisation/UiOrganisationUsers.vue'),
            },
            {
                path: 'organisations',
                props: true,
                name: 'organisations-list',
                component: () => import(/* webpackChunkName: "OrganisationList" */ '@/components/organisation/UiOrganisationList.vue'),
            },
            {
                path: 'new-organisation',
                props: true,
                name: 'organisation-creation',
                component: () => import(/* webpackChunkName: "orgNew" */ '@/components/organisation/UiOrganisationNew.vue'),
            },
            {
                path:'profile',
                /*props: (route: { params: { userId: Number; }; }) => ({ userId: Number(store.getters.userId) }),*/
                props: true,
                name: 'user-profile',
                component: () => import(/* webpackChunkName: "userProfile" */ '@/pages/PageUserProfile.vue'),
                children: [
                    {
                        path:':userId',
                        props: true,
                        component: () => import(/* webpackChunkName: "userProfile" */ '@/pages/PageUserProfile.vue'),
                    }
                ]
            }
        ]
    },
    {
        path: '/about',
        props: true,
        name: 'impressum',
        component: () => import(/* webpackChunkName: "impressum" */ '@/pages/PageImpressum.vue'),
    },
    {
        path: '/login',
        props: true,
        name: 'login',
        meta: { notAuthRequired: true },
        component: () => import(/* webpackChunkName: "login" */ '@/pages/PageLogin.vue'),
    },
    {
        path: '/register',
        props: true,
        name: 'user-registration',
        meta: { notAuthRequired: true },
        component: () => import(/* webpackChunkName: "register" */ '@/pages/PageRegister.vue'),
    },
    {
        path: '/activate/:tokenId',
        props: true,
        name: 'user-activation',
        meta: { notAuthRequired: true },
        component: () => import(/* webpackChunkName: "activate" */ '@/pages/PageLogin.vue'),
    },
    {
        path: '/password-reset/:tokenId',
        props: true,
        name: 'password-reset',
        meta: { notAuthRequired: true },
        component: () => import(/* webpackChunkName: "password-reset" */ '@/pages/PagePasswordReset.vue'),
    },
    {
        path: '/maintenance',
        props: true,
        name: 'maintenance',
        meta: { notAuthRequired: true },
        component: () => import(/* webpackChunkName: "maintenance" */ '@/pages/PageMaintenance.vue'),
    },
];

export default routes;