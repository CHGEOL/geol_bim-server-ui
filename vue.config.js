// vue.config.js
const path = require('path');

module.exports = {
  publicPath: '/',
  // publicPath: process.env.NODE_ENV === 'fhnw-staging' || process.env.NODE_ENV === 'fhnw-development'
  // ? '/bim-profsrv/'
  // : '/',

  pluginOptions: {
    'style-resources-loader': {
      preProcessor: 'less',
      patterns: [
        path.resolve(__dirname, './src/styles/*.less'),
      ]
    }
  },
  transpileDependencies: [
    'vuex-module-decorators'
  ],
  configureWebpack: {
    plugins: [
    ],
    output: {
      filename: 'api.[name].[hash].js'
    }
  }
}
  
